<?php

namespace Bss\CustomProfileConf\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Catalog\Model\Product;

class UpgradeData implements UpgradeDataInterface {

    /**
     * Upgrades data for a module
     *Bss\CustomProfileConf\Model\Attribute\Backend\
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    protected $eavFactory;

    public function __construct(\Magento\Eav\Setup\EavSetupFactory $eavFactory)
    {
        $this->eavFactory = $eavFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '1.2.0', '<')) {

            $eavSetup = $this->eavFactory->create();
            $eavSetup->addAttribute(
                Product::ENTITY,
                'new_attr_test',
                [
                    'group' => 'General',
                    'type' => 'varchar',
                    'label' => 'Clothing Material',
                    'input' => 'select',
                    'backend' => Backend::class,
                    'frontend' => Frontend::class,
                    'required' => false,
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'is_used_in_grid' => false,
                    'is_visible_in_grid' => false,
                    'is_filterable_in_grid' => false,
                    'visible' => true,
                    'is_html_allowed_on_front' => true,
                    'visible_on_front' => true,
                    'source' => Source::class,
                    'sort_order' => 10,
                    'apply_to' => 'simple,configurable,virtual,bundle,downloadable,grouped'
                ]
            );
        }
        $setup->endSetup();

    }
}
