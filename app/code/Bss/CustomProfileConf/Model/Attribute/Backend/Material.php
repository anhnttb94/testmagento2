<?php

namespace Bss\CustomProfileConf\Model\Attribute\Backend;

use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Attribute\Backend\AbstractBackend;


class Material extends AbstractBackend {
    public function validate($object)
    {
        $value = $object->getData($this->getAttribute()->getAttributeCode());
        if(($object->getAttributeSetId == 10) && ($value == 'fur')) {
            throw new \Exception(__('Bottom can not be fur'));
        }
    }
}
