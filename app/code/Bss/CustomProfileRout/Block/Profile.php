<?php

namespace Bss\CustomProfileRout\Block;

use Bss\CustomProfileRout\Helper\Profile\ProfileDefault;

class Profile extends \Magento\Framework\View\Element\Template {
    public function __construct (
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    public function getDefaultName() {
        return ProfileDefault::DF_NAME;
    }

    public function getDefaultAge() {
        return ProfileDefault::DF_AGE;
    }

    public function getDefaultDateBirth() {
        return ProfileDefault::DF_DATEBIRTH;
    }
}


