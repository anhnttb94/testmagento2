<?php

namespace Bss\CustomProfile\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\Message\ManagerInterface;

class InternshipProfilePostObserver implements ObserverInterface {

    private $customerSession;

    private $messageManager;

    private $responseFactory;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    private $url;

    public function __construct(Session $customerSession, ManagerInterface $messageManager,
                                \Magento\Framework\App\ResponseFactory $responseFactory,
                                \Magento\Framework\UrlInterface $url
    )
    {
        $this->customerSession = $customerSession;
        $this->messageManager = $messageManager;
        $this->responseFactory = $responseFactory;
        $this->url = $url;
    }

    private function isLoggedIn() {
        return $this->customerSession->isLoggedIn();
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        // TODO: Implement execute() method.
        if(!$this->isLoggedIn()) {
            $url = $this->url->getUrl('customer/account/login');
            $observer->getControllerAction()->getResponse()
                ->setRedirect($url);
        }
    }
}
