/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            profileAdd: 'Bss_CustomProfile/js/profile-add',
            profileViewSlider: 'Bss_CustomProfile/js/profile-slider'
        }
    },
    paths: {
        'bootstrap':'Bss_CustomProfile/js/bootstrap.bundle',
        'owlcarousel': 'Bss_CustomProfile/js/owl.carousel'
    } ,
    shim: {
        'bootstrap': {
            'deps': ['jquery']
        },
        'owlcarousel': {
            'deps' : ['jquery']
        }
    }
};

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

