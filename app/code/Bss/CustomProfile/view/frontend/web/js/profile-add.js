define([
    'jquery',
    'jquery-ui-modules/widget'
], function ($) {
    'use strict';
    $.widget('mage.profileAdd', {
        options: {
            minWidth: 300, // Minimum width of the gallery image.
            widthOffset: 90, // Offset added to the width of the gallery image.
            heightOffset: 210, // Offset added to the height of the gallery image.
            closeWindow: 'div.buttons-set a[role="close-window"]' // Selector for closing the gallery popup window.
        },

        /**
         * Bind click handler for closing the popup window and resize the popup based on the image size.
         * @private
         */
        _create: function () {
            console.log("2323232323", this);
        },

    });

    $(document).on('click', '#submitProfile', function () {
        let fullName = $('#fullName')[0].value;
        let dob = $('#dob')[0].value;
        let age = $('#age')[0].value;

        $.ajax({
            url: '/magento/profile/profile/addprofile',
            type: 'POST',
            data: {
                fullName,
                dob,
                age
            },
            dataType: 'json'
        }).done(function(data) {
            console.log("123456", data);
        })
    })

    return $.mage.profileAdd;
});

