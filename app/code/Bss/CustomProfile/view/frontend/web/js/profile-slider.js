define([
    'jquery',
    'jquery-ui-modules/widget',
    'owlcarousel'
], function ($) {
    'use strict';
    $.widget('mage.profileViewSlider', {

        _create: function () {
            console.log("123123123");

        },
    });

    $('.owl-carousel').owlCarousel({
        autoPlay: 3000,
        margin:5,
        items : 5,
        itemsDesktop : [1199,5],
        itemsDesktopSmall : [979,5],
        itemsTablet : [768,5],
        navigation : true,
        pagination : false,
        loop: true
    });

    return $.mage.profileViewSlider;
});
