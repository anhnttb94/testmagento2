<?php

namespace Bss\CustomProfile\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface {

    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        // TODO: Implement install() method.
        $setup->startSetup();
        $conn = $setup->getConnection();
        $tableName = $setup->getTable('internship_profile');
        if($conn->isTableExists($tableName) != true) {
            $table = $conn->newTable($tableName)
                            ->addColumn('id', Table::TYPE_INTEGER, null, ['nullable' => false, 'primary' => true, 'unsigned' => true, 'auto_increment' => true])
                            ->addColumn('full_name', Table::TYPE_TEXT, 255, ['nullable' => false])
                            ->addColumn('age', Table::TYPE_INTEGER, 10, ['nullable' => false])
                            ->addColumn('date_of_birth', Table::TYPE_TEXT, 255, ['nullable' => false]);
            $conn->createTable($table);
        }
        $setup->endSetup();
    }
}

