<?php

namespace Bss\CustomProfile\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface {
    /**
     * Upgrades DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        // TODO: Implement upgrade() method.
        $installer = $setup;
        $installer->startSetup();

        if(version_compare($context->getVersion(), '1.1.0', '<')) {
            if (!$installer->tableExists('internship_profile')) {
                $table = $installer->getConnection()->newTable($installer->getTable('internship_profile'))
                    ->addColumn('id', Table::TYPE_INTEGER, null, ['nullable' => false, 'primary' => true, 'unsigned' => true, 'auto_increment' => true])
                    ->addColumn('full_name', Table::TYPE_TEXT, 255, ['nullable' => false])
                    ->addColumn('age', Table::TYPE_INTEGER, 10, ['nullable' => false])
                    ->addColumn('date_of_birth', Table::TYPE_TEXT, 255, ['nullable' => false]);
                $installer->getConnection()->createTable($table);
            }
        }
        $installer->endSetup();
    }
}
