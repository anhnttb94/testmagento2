<?php
namespace Bss\CustomProfile\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallData implements InstallSchemaInterface
{

    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        // TODO: Implement install() method.
        $data = [
            [
                'full_name'=> 'Nguyen Trung Anh', 'age' => 26, 'date_of_birth' => '28/11/1994'
            ],
            [
                'full_name'=> 'Nguyen Hieu Anh', 'age' => 28, 'date_of_birth' => '1/1/1992'
            ],
            [
                'full_name'=> 'Nguyen Tung Anh', 'age' => 28, 'date_of_birth' => '1/2/1992'
            ],
            [
                'full_name'=> 'Nguyen Van Anh', 'age' => 28, 'date_of_birth' => '1/3/1992'
            ],
            [
                'full_name'=> 'Nguyen Duc Anh', 'age' => 28, 'date_of_birth' => '1/4/1992'
            ]
        ];

        foreach ($data as $bind) {
            $setup->getConnection()->insertForce($setup->getTable('internship_profile'), $bind);
        }
    }
}
