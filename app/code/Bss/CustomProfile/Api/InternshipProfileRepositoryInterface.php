<?php

namespace Bss\CustomProfile\Api;

use Bss\CustomProfile\Api\Data\InternshipProfileInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface InternshipProfileRepositoryInterface {
    /**
     * @param int $id
     * @return \Bss\CustomProfile\Api\Data\InternshipProfileInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById ($id);

    /**
     * @param \Bss\CustomProfile\Api\Data\InternshipProfileInterface $internshipProfile
     * @return \Bss\CustomProfile\Api\Data\InternshipProfileInterface
     */
    public function save(InternshipProfileInterface $internshipProfile);

    /**
     * @param \Bss\CustomProfile\Api\Data\InternshipProfileInterface $internshipProfile
     * @return void
     */
    public function delete (InternshipProfileInterface $internshipProfile);

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Bss\CustomProfile\Api\Data\InternshipProfileSearchResultInterface
     */
    public function getList (SearchCriteriaInterface $searchCriteria);
}
