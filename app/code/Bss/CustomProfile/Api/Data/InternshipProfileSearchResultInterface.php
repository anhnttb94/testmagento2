<?php

namespace Bss\CustomProfile\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface InternshipProfileSearchResultInterface extends SearchResultsInterface {
    /**
     * @return \Bss\CustomProfile\Api\Data\InternshipProfileInterface[]
     */
    public function getItems();

    /**
     * @param \Bss\CustomProfile\Api\Data\InternshipProfileInterface[] $items
     * @return void
     */
    public function setItems(array $items);
}
