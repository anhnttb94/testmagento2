<?php

namespace Bss\CustomProfile\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface as ExtensibleDataInterfaceAlias;

interface InternshipProfileInterface extends ExtensibleDataInterfaceAlias
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return string
     */
    public function getFullName();

    /**
     * @param string $name
     * @return void
     */
    public function setFullName($name);

    /**
     * @return int
     */
    public function getAge();

    /**
     * @param int $age
     * @return void
     */
    public function setAge($age);

    /**
     * @return string
     */
    public function getDateOfBirth();

    /**
     * @param string $dob
     * @return void
     */
    public function setDateOfBirth($dob);

    /**
     * @return \Bss\CustomProfile\Api\Data\InternshipProfileExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * @param \Bss\CustomProfile\Api\Data\InternshipProfileExtensionInterface $extensionAttributes
     * @return void
     */
    public function setExtensionAttributes(InternshipProfileExtensionInterface $extensionAttributes);

}
