<?php

namespace Bss\CustomProfile\Model;
use Bss\CustomProfile\Api\Data\InternshipProfileInterface;
use Bss\CustomProfile\Api\InternshipProfileRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\NoSuchEntityException;
use Bss\CustomProfile\Model\ResourceModel\InternshipProfile\CollectionFactory as IntershipCollectionFactory;
use Bss\CustomProfile\Model\ResourceModel\InternshipProfile\Collection;
use Bss\CustomProfile\Api\Data\InternshipProfileSearchResultInterface;
use Bss\CustomProfile\Api\Data\InternshipProfileSearchResultInterfaceFactory;

class InternshipProfileRepository implements InternshipProfileRepositoryInterface{

    private $internshipProfileFactory;

    private $intershipProfileCollectionFactory;

    private $searchResultFactory;

    public function __construct(
        \Bss\CustomProfile\Model\InternshipProfileFactory $internshipProfileFactory,
        IntershipCollectionFactory $intershipProfileCollectionFactory,
        InternshipProfileSearchResultInterfaceFactory $searchResultFactory
    )
    {
        $this->internshipProfileFactory = $internshipProfileFactory;
        $this->intershipProfileCollectionFactory = $intershipProfileCollectionFactory;
        $this->searchResultFactory = $searchResultFactory;
    }

    /**
     * @param int $id
     * @return \Bss\CustomProfile\Api\Data\InternshipProfileInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($id)
    {
        // TODO: Implement getById() method.
        $internshipProfile = $this->internshipProfileFactory->create();
        $internshipProfile->getResource()->load($internshipProfile, $id);
        if (!$internshipProfile->getId()) {
            throw new NoSuchEntityException(__('Unable to find hamburger with ID "%1"', $id));
        }
        return $internshipProfile;
    }

    /**
     * @param \Bss\CustomProfile\Api\Data\InternshipProfileInterface $internshipProfile
     * @return \Bss\CustomProfile\Api\Data\InternshipProfileInterface
     */
    public function save(InternshipProfileInterface $internshipProfile)
    {
        // TODO: Implement save() method.
        $internshipProfile->getResource()->save($internshipProfile);
        return $internshipProfile;
    }

    /**
     * @param \Bss\CustomProfile\Api\Data\InternshipProfileInterface $internshipProfile
     * @return void
     */
    public function delete(InternshipProfileInterface $internshipProfile)
    {
        // TODO: Implement delete() method.
        $internshipProfile->getResource()->delete($internshipProfile);
    }

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Bss\CustomProfile\Api\Data\InternshipProfileSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        // TODO: Implement getList() method.
        $collection = $this->intershipProfileCollectionFactory->create();
        $this->addFiltersToCollection($searchCriteria, $collection);
        $this->addSortOrdersToCollection($searchCriteria, $collection);
        $this->addPagingToCollection($searchCriteria, $collection);

        $collection->load();

        return $this->buildSearchResult($searchCriteria, $collection);

    }

    private function addFiltersToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection) {
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            $fields = $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                $fields[] = $filter->getField();
                $conditions[] = [$filter->getConditionType() => $filter->getValue()];
            }
            $collection->addFieldToFilter($fields, $conditions);
        }
    }

    private function addSortOrdersToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        foreach ((array) $searchCriteria->getSortOrders() as $sortOrder) {
            $direction = $sortOrder->getDirection() == SortOrder::SORT_ASC ? 'asc' : 'desc';
            $collection->addOrder($sortOrder->getField(), $direction);
        }
    }

    private function addPagingToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        $collection->setPageSize($searchCriteria->getPageSize());
        $collection->setCurPage($searchCriteria->getCurrentPage());
    }

    private function buildSearchResult(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        $searchResults = $this->searchResultFactory->create();

        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }
}
