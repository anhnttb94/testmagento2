<?php

namespace Bss\CustomProfile\Model\ResourceModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

class InternshipProfile extends AbstractDb {
    /**
     * Resource initialization
     *
     * @return void
     */
    public function __construct(
        Context $context
    )
    {
        parent::__construct($context);
    }

    protected function _construct()
    {
        $this->_init('internship_profile', 'id');
        // TODO: Implement _construct() method.
    }


}
