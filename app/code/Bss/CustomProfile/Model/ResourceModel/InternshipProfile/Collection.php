<?php

namespace Bss\CustomProfile\Model\ResourceModel\InternshipProfile;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'internship_profile_collection';
    protected $_eventObject = 'internship_profile_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Bss\CustomProfile\Model\InternshipProfile', 'Bss\CustomProfile\Model\ResourceModel\InternshipProfile');
    }
}
