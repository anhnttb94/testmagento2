<?php

namespace Bss\CustomProfile\Model;
use Magento\Framework\Api\SearchResults;
use Bss\CustomProfile\Api\Data\InternshipProfileSearchResultInterface;

class InternshipProfileSearchResult implements InternshipProfileSearchResultInterface
{

    /**
     * @return \Bss\CustomProfile\Api\Data\InternshipProfileInterface[]
     */
    public function getItems()
    {
        // TODO: Implement getItems() method.
    }

    /**
     * @param \Bss\CustomProfile\Api\Data\InternshipProfileInterface[] $items
     * @return void
     */
    public function setItems(array $items)
    {
        // TODO: Implement setItems() method.
    }

    /**
     * Get search criteria.
     *
     * @return \Magento\Framework\Api\SearchCriteriaInterface
     */
    public function getSearchCriteria()
    {
        // TODO: Implement getSearchCriteria() method.
    }

    /**
     * Set search criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return $this
     */
    public function setSearchCriteria(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        // TODO: Implement setSearchCriteria() method.
    }

    /**
     * Get total count.
     *
     * @return int
     */
    public function getTotalCount()
    {
        // TODO: Implement getTotalCount() method.
    }

    /**
     * Set total count.
     *
     * @param int $totalCount
     * @return $this
     */
    public function setTotalCount($totalCount)
    {
        // TODO: Implement setTotalCount() method.
    }
}
