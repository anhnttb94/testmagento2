<?php

namespace Bss\CustomProfile\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\DataObject\IdentityInterface;
use Bss\CustomProfile\Api\Data\InternshipProfileInterface;
use Magento\Framework\Model\AbstractExtensibleModel;

class InternshipProfile extends AbstractExtensibleModel implements IdentityInterface,InternshipProfileInterface  {

    /**
     * Return unique ID(s) for each object in system
     *
     * @return string[]
     */
    const CACHE_TAG = 'internship_profile';
    protected $_cacheTag = 'internship_profile';
    protected $_eventPrefix = 'internship_profile';

    const NAME = 'full_name';
    const DOB = 'date_of_birth';
    const AGE = 'age';

    protected function _construct()
    {
        $this->_init('\Bss\CustomProfile\Model\ResourceModel\InternshipProfile');
    }


    public function getIdentities()
    {
        // TODO: Implement getIdentities() method.
        return [$this->getId()];
    }

    public function getDefaultValue() {
        $values = [];

        return $values;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        // TODO: Implement getFullName() method.
        return $this->getData(self::NAME);
    }

    /**
     * @param string $name
     * @return void
     */
    public function setFullName($name)
    {
        // TODO: Implement setFullName() method.
        $this->setData(self::NAME, $name);
    }

    /**
     * @return int
     */
    public function getAge()
    {
        // TODO: Implement getAge() method.
        return $this->getData(self::AGE);
    }

    /**
     * @param int $age
     * @return void
     */
    public function setAge($age)
    {
        // TODO: Implement setAge() method.
        $this->setData(self::AGE, $age);
    }

    /**
     * @return string
     */
    public function getDateOfBirth()
    {
        // TODO: Implement getDateOfBirth() method.
        return $this->getData(self::DOB);
    }

    /**
     * @param string $dob
     * @return void
     */
    public function setDateOfBirth($dob)
    {
        // TODO: Implement setDateOfBirth() method.
        $this->setData(self::DOB, $dob);
    }

    /**
     * @return \Bss\CustomProfile\Api\Data\InternshipProfileExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        // TODO: Implement getExtensionAttributes() method.
        return $this->_getExtensionAttributes();
    }

    /**
     * @param \Bss\CustomProfile\Api\Data\InternshipProfileExtensionInterface $extensionAttributes
     * @return void
     */
    public function setExtensionAttributes(\Bss\CustomProfile\Api\Data\InternshipProfileExtensionInterface $extensionAttributes)
    {
        // TODO: Implement setExtensionAttributes() method.
        $this->_setExtensionAttributes($extensionAttributes);
    }
}
