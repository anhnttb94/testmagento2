<?php

namespace Bss\CustomProfile\Plugin;

class TestPlugin {
    public function beforeSetTitle(\Bss\CustomProfile\Controller\Profile\Test $subject, $title) {
        $title = $title . " to ";
        echo __METHOD__ . "</br>";

        return [$title];
    }
}
