<?php

namespace Bss\CustomProfile\Plugin;

use Magento\Customer\Model\Session;

class RedirectProductViewBeforeLoggedInPlugin {

    private $customerSession;
    public function __construct(Session $customerSession)
    {
        $this->customerSession = $customerSession;
    }

    private function isLoggedIn () {
        return $this->customerSession->isLoggedIn();
    }

    public function beforeSetErrMessage( \Bss\CustomProfile\Observer\InternshipProfileObserver $subject, $message) {
        if($this->isLoggedIn()) {
            $message = "Dear mr, " . $message;
        }
        else {
            $message = "Dear guest, " . $message;
        }
        return [$message];
    }
}
