<?php

namespace Bss\CustomProfile\Block;

use Bss\CustomProfile\Helper\Profile\ProfileDefault;

class Profile extends \Magento\Framework\View\Element\Template {

    protected $internshipProfileFactory;
    public function __construct (
        \Magento\Framework\View\Element\Template\Context $context,
        \Bss\CustomProfile\Model\InternshipProfileFactory $internshipProfileFactory,
        array $data = []
    ) {
        $this->internshipProfileFactory = $internshipProfileFactory;
        parent::__construct($context, $data);
    }

    public function getDefaultName() {
        return ProfileDefault::DF_NAME;
    }

    public function getDefaultAge() {
        return ProfileDefault::DF_AGE;
    }

    public function getDefaultDateBirth() {
        return ProfileDefault::DF_DATEBIRTH;
    }

    public function getAllProfiles () {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $profile = $this->internshipProfileFactory->create();
        $coll = $profile->getCollection();
        $logger->info($coll->toArray());
//        $collection = $profile->getCollection();
        return $coll;
    }
}


