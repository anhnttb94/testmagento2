<?php

namespace  Bss\CustomProfile\Block;

class ProfileSlider extends \Magento\Framework\View\Element\Template {
    protected $internshipProfileFactory;
    public function __construct (
        \Magento\Framework\View\Element\Template\Context $context,
        \Bss\CustomProfile\Model\InternshipProfileFactory $internshipProfileFactory,
        array $data = []
    ) {
        $this->internshipProfileFactory = $internshipProfileFactory;
        parent::__construct($context, $data);
    }

    public function getAllProfiles () {
        $profile = $this->internshipProfileFactory->create();
        $coll = $profile->getCollection();
//        $collection = $profile->getCollection();
        return $coll;
    }
}
