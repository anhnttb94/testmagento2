<?php

namespace Bss\CustomProfile\Controller\Page;
use Bss\CustomProfile\Helper\Profile\ProfileDefault;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;


class View extends Action {


    protected $resultPageFactory;
    public function __construct(Context $context, PageFactory $pageFactory)
    {
        $this->resultPageFactory = $pageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
    }
}
