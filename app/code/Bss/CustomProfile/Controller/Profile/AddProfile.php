<?php

namespace Bss\CustomProfile\Controller\Profile;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Bss\CustomProfile\Model\InternshipProfileFactory;

class AddProfile extends Action implements CsrfAwareActionInterface {
    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    protected $_resultJsonFactory;
    protected $_internshipProfileFactory;

    public function __construct(Context $context, JsonFactory $resultJsonFactory, InternshipProfileFactory $_internshipProfileFactory)
    {
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_internshipProfileFactory = $_internshipProfileFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        try {
            $result = $this->_resultJsonFactory->create();
            $fullName = $this->getRequest()->getParam('fullName');
            $dob = $this->getRequest()->getParam('dob');
            $age = $this->getRequest()->getParam('age');
            $profile = $this->_internshipProfileFactory->create();
            $profile->setData(['full_name'=> $fullName, 'age'=> $age, 'date_of_birth'=> $dob]);
            $profile->save();
            $response = ['status'=> 'ok', 'msg'=> 'add success'];
            return $result->setData($response);
        }
        catch (\Exception $e) {
            throw $e;
        }

        // TODO: Implement execute() method.
    }

    /**
     * Create exception in case CSRF validation failed.
     * Return null if default exception will suffice.
     *
     * @param RequestInterface $request
     *
     * @return InvalidRequestException|null
     */
    public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException
    {
        return null;
        // TODO: Implement createCsrfValidationException() method.
    }

    /**
     * Perform custom request validation.
     * Return null if default validation is needed.
     *
     * @param RequestInterface $request
     *
     * @return bool|null
     */
    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return true;
        // TODO: Implement validateForCsrf() method.
    }
}
