<?php

namespace Bss\CustomProfile\Controller\Profile;

use Magento\Framework\App\ResponseInterface;
use Bss\CustomProfile\Helper\Profile\ProfileDefault;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;


class CustomProfileController extends Action {
    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */

    protected $resultJsonFactory;
    public function __construct(Context $context, JsonFactory $resultJsonFactory)
    {
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        // TODO: Implement execute() method.
        $result = $this->resultJsonFactory->create();
        $data = ['Name' => ProfileDefault::DF_NAME, 'AGE'=> ProfileDefault::DF_AGE, 'DateBirth' => ProfileDefault::DF_DATEBIRTH];
        return $result->setData($data);
    }
}
