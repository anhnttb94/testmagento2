<?php

namespace Bss\CustomProfile\Controller\Profile;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;

class Test extends Action {

    protected $title;

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function __construct(Context $context)
    {
        parent::__construct($context);
    }

    public function getTitle() {
        return $this->title;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function execute()
    {
        // TODO: Implement execute() method.
        echo $this->setTitle('tada');
        echo $this->getTitle();
    }
}
